import { Injectable } from '@angular/core';
import { Plant } from './plant';

const plants: Plant [] =[
  new Plant ('Лаванда','111',139.0),
  new Plant ('Фиалка','222',122.0),
  new Plant ('Цикломент','333',199.0),
]

@Injectable({
  providedIn: 'root'
})
export class PlantService {
  getPlants(): Plant[] {return plants;}

}
