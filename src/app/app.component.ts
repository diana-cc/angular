import { Component } from '@angular/core';
import {NewServiceService} from "./new-service.service";
import {HttpClient} from "@angular/common/http";
import {Plant} from './plant';
import { PlantService } from './plant.service';
import { OnInit } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl:'./app.component.html',
  styleUrls :['./app.component.css'],

  // template: `<div>
  //   <h1>Приложение блокнот</h1>
  //   <nav>
  //     <a routerLink="">Главная</a>
  //     <a routerLink="/home">Главная</a>
  //     <a routerLink="/about">о нас</a>
  //     <a routerLink="/news">Главная</a>
  //     <a routerLink="/prase">Главная</a>
  //     <a routerLink="/not-found">Главная</a>
  //   </nav>
  //   <router-outlet></router-outlet>
  // </div>`

})
export class AppComponent implements OnInit{
  title = 'my-dream-app';
  text = 'test';
  useName: string = "";
  response: any;
  plant:Plant = new Plant('Лаванда','h', 99.9);
  temp:Plant = new Plant('Лаванда', 'h', 99.9);
  plants:Plant[];
ngOnInit(){
  this.plants = this.ps.getPlants();
}


  constructor(private  ps:PlantService) {

  }
  save(): void{
    this.plant.name = this.temp.name;
    this.plant.description = this.temp.description;
    this.plant.price = this.temp.price;
  }

  cancel(): void {
    this.temp.name = this.plant.name;
    this.temp.description = this.plant.description;
    this.temp.price = this.plant.price;
  }


}
