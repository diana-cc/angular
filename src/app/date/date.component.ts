import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DateComponent implements OnInit {
  message: string = new Date().toDateString();
  message1: string;
  items= [ ];
  loggedIn = true;
  text:string = " ";
  isCollapsed = false ;
  isCollapsed2= false;



  constructor() {
    setInterval(()=>{
      this.message1 = new Date().toLocaleTimeString();
    },1000);
  }
  myClick(event){
    this.isCollapsed = !this.isCollapsed;

  }
  myClick2(event){
    this.isCollapsed2 = true;
    let testObj = {
      text: this.text + ' ' + this.message1,
      checkboxState: false,
    };
    this.items.push(testObj);


  }
  myClick3(event){


  }
  ngOnInit() {
  }

}
