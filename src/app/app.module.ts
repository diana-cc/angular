import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {HttpClient } from '@angular/common/http'; //это уже готовыуе модули инструменты

import { FormsModule } from "@angular/forms";

import { Routes, RouterModule } from "@angular/router";

import { AppComponent } from './app.component';
import { DateComponent } from './date/date.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { NewsComponent } from './news/news.component';
import { PraseComponent } from './prase/prase.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NewServiceService } from './new-service.service';
import { PlantService } from './plant.service';
import { PlantDetailComponent } from './plant-detail.component';




const appRoutes: Routes = [
  {path:'', component:DateComponent},
{path:'home', component:HomeComponent},
{path:'about', component:AboutComponent},
{path:'news', component:NewsComponent},
{path:'prase', component:PraseComponent},
{path:'**', component:NotFoundComponent},

]

@NgModule({
  declarations: [

    AppComponent,
    DateComponent,
    HomeComponent,
    AboutComponent,
    NewsComponent,
    PraseComponent,
    NotFoundComponent,
    PlantDetailComponent//компоненты содержащие приложения
  ],
  imports: [
    BrowserModule,//модули использующие приложение
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [NewServiceService,
    PlantService],
    //Сервисы отвечающие по запросам

  bootstrap: [AppComponent]//компоненты содержащие приложения
})
export class AppModule { }
