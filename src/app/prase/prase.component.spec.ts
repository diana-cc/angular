import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PraseComponent } from './prase.component';

describe('PraseComponent', () => {
  let component: PraseComponent;
  let fixture: ComponentFixture<PraseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PraseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PraseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
